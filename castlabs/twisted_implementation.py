"""
implementation of proxy on top of twisted
"""
import json
import logging
import sys
import datetime
import os


from twisted.internet import reactor
from twisted.python import log
import twisted.web.http
import twisted.web.proxy
from twisted.python.compat import urllib_parse
from twisted.web.proxy import ProxyRequest


log.startLogging(sys.stdout)


# noinspection PyIncorrectDocstring
def json_dump(body: object) -> str:
    """transform python object into json"""

    # noinspection PyIncorrectDocstring
    def default(data):
        """serialization for custom type like bytes"""
        if isinstance(data, bytes):
            return data.decode("utf-8")

    return json.dumps(
        body,
        indent=4,
        default=default,
        sort_keys=True
    ).encode("utf-8")


# TODO http://twistedmatrix.com/documents/12.0.0/core/howto/logging.html
# add logging
class MyProxyRequest(ProxyRequest):
    """
    proxy wrapper for custom business
    """
    _status_transferred_bytes = 0
    _status_uptime = datetime.datetime.utcnow()

    def _fail(self, body: dict):
        self.setResponseCode(
            body["error"]["code"],
            body["error"]["message"].encode("utf-8")
        )

        self.responseHeaders.addRawHeader(b"Content-Type", b"text/json")

        self.write(json_dump(body))
        self.finish()

    def _ok(self, body: dict):
        self.setResponseCode(
            body["ok"]["code"],
            body["ok"]["message"].encode("utf-8")
        )

        self.responseHeaders.addRawHeader(b"Content-Type", b"text/json")

        self.write(json_dump(body))
        self.finish()

    # noinspection PyIncorrectDocstring
    def write(self, data):
        # pylint: disable=method-hidden
        """
        data can be write in a few chunks
        in this case better to append to transferred traffic
        each chunk
        so self.responseHeaders["content-length"] is not a variant
        """
        ProxyRequest.write(self, data)
        self.__class__._status_transferred_bytes += len(data)

    def process(self):
        # pylint: disable=protected-access
        log.msg("self.uri %s" % self.uri, logLevel=logging.DEBUG)

        parsed = urllib_parse.urlparse(self.uri)

        if not parsed.scheme:
            if parsed.path == b"/status":
                self._ok({
                    "ok": {
                        "code": 200,
                        "message": "ok"
                    },
                    "transferred_bytes":
                        self.__class__._status_transferred_bytes,
                    "uptime": str(
                        datetime.datetime.utcnow() -
                        self.__class__._status_uptime
                    )
                })
                return

            self._fail({
                "error": {
                    "code": 404,
                    "message": "Not Found"
                },
                "path": parsed.path
            })
            return

        range_from_query = urllib_parse.parse_qs(
            parsed.query
        ).get(b"range", [None])[0]

        protocol = parsed[0]
        host = parsed[1].decode('ascii')
        port = self.ports[protocol]
        if ':' in host:
            host, port = host.split(':')
            port = int(port)
        rest = urllib_parse.urlunparse((b'', b'') + parsed[2:])
        if not rest:
            rest = rest + b'/'
        class_ = self.protocols[protocol]
        headers = self.getAllHeaders().copy()

        log.msg("request.headers bef %s" % headers, logLevel=logging.DEBUG)
        range_from_header = headers.get(
            b"range", b""
        ).replace(
            b"bytes=", b""
        )

        # from specs RFC2616
        # The only range unit defined by HTTP/1.1 is "bytes". HTTP/1.1
        # implementations MAY ignore ranges specified using other units.
        if range_from_query:
            if (range_from_header and
                    range_from_query != range_from_header):

                message = "both header and query parameter are specified, "\
                          "but with different value"

                self._fail(
                    {
                        "error": {
                            "message": message,
                            "code": 413,
                            "range_from_query": range_from_query,
                            "range_from_header": range_from_header
                        }
                    }
                )
                return
            else:
                headers[b"range"] = b"bytes=" + range_from_query

        log.msg("request.headers after %s" % headers, logLevel=logging.DEBUG)
        if b'host' not in headers:
            headers[b'host'] = host.encode('ascii')
        self.content.seek(0, 0)
        content = self.content.read()
        client_factory = class_(
            self.method, rest, self.clientproto, headers, content, self
        )

        # pylint: disable=no-member
        self.reactor.connectTCP(host, port, client_factory)


# noinspection PyClassHasNoInit
class MyProxy(twisted.web.http.HTTPChannel):
    """A receiver for HTTP requests."""
    requestFactory = MyProxyRequest


# noinspection PyClassHasNoInit
class ProxyFactory(twisted.web.http.HTTPFactory):
    """Factory for HTTP server."""
    protocol = MyProxy


# noinspection PyIncorrectDocstring
def run(port=8841):
    """start proxy server"""
    reactor.listenTCP(port, ProxyFactory())  # pylint: disable=no-member
    reactor.run()  # pylint: disable=no-member


if __name__ == "__main__":
    try:
        # check if port is set via ENV after that via CMD
        run(int(os.environ.get("PROXY_PORT") or sys.argv[1]))
    except IndexError:
        run()
    except ValueError as value_error:
        print(
            "Oops!  That was no valid number `%s`.  Try again..." %
            value_error
            )
        exit(1)

