====================================
AsyncProxy - Python Programming Task
====================================

My task is to build an asynchronous HTTP proxy complying to the requirements
specified below.

Requirements:
=============

1. Range requests support as defined in
   [RFC2616](https://www.ietf.org/rfc/rfc2616.txt), but also via `range` query
   parameter.

   f.e. Nominatim-2.4.0.tar.bz2 is about 30Mb

   *By using headers*

   .. code-block:: bash

       $ curl -x http://127.0.0.1:8841 \
            --range 0-99 http://www.nominatim.org/release/Nominatim-2.4.0.tar.bz2

   *By using `range` query*

   .. code-block:: bash

       $ curl -x http://proxy_server:proxy_port \
            http://www.nominatim.org/release/Nominatim-2.4.0.tar.bz2?range=0-99

2. Return 416 error in case where both header and query parameter are
   specified, but with different value.

   .. code-block:: bash

        $ curl -x http://proxy_server:proxy_port \
              --range 0-100 http://www.nominatim.org/release/Nominatim-2.4.0.tar.bz2?range=0-99
        416 {"error": "where both header and query parameter are specified, but with different value"}

3. Expose proxy statistics at /stats endpoint (total bytes transferred, uptime).

   .. code-block:: bash

       $ curl http://proxy_server:proxy_port/stats
       {"transferred": "100Mb", "uptime": "100day"}

4. Proxy should be delivered with appropriate `Dockerfile` and
   `docker-compose.yml` (Hint: use python:3.5 image).

   .. code-block:: bash

        $ docker build -t castlabs/proxy:0.1 .

        $ docker run -t -i -e PROXY_PORT=8098 \
            --name castlabs_proxy castlabs/proxy:0.1

        $ docker-compose up

5. Proxy should be configurable via environmental variables.

   .. code-block:: bash

       $ docker run -t -i -e PROXY_PORT=8098 \
            --name castlabs_proxy castlabs/proxy:0.1

   .. code-block:: bash

        $ docker-compose up


Implementation:
===============

The solutions can be:
    - SimpleHTTPServer from python battery (not implemented)

       reject: is not async by default, threads or processes require for non blocking

    - tornado (not implemented)

        is ok, I did not used it before

    - twisted (implemented)

        is ok, there is implemented in this repo

    - CherryProxy (not implemented)

        a filtering HTTP proxy extensible in Python

        must customise lib for castlabs specs

        https://bitbucket.org/decalage/cherryproxy/src/1e55ae33fb5edb0d60accc52f8519e9dac6ce413/cherryproxy/README.txt?at=default&fileviewer=file-view-default

    - django-http-proxy (not implemented)

        (plus) in case if there is already on server django install is a good reuse of stack solution

        (minus) is require django infrastructure (a lot of additional layers so degradation in performance)

        must customise lib for castlabs specs
        http://django-http-proxy.readthedocs.org/en/stable/introduction.html




Got example from:

    https://wiki.python.org/moin/Twisted-Examples

How to test with curl:

    http://www.cyberciti.biz/cloud-computing/http-status-code-206-commad-line-test/


Installation
============

Apt-get
```````

Install all apt-get requirements

.. code-block:: bash

    $ sudo apt-get install -y --force-yes \
        $(grep -oP "^[^#\s]+" requirements-apt-get.txt)


Pip
```

Update default ``pip``

.. code-block:: bash

    $ sudo pip install --upgrade pip virtualenv bpython

Make virtualenv

.. code-block:: bash

    $ sudo virtualenv -p python3 /_opt/python-env/castlabs

Install requirements into virtualenv

.. code-block:: bash

    $ sudo /_opt/python-env/castlabs/bin/pip install \
        -r requirements-pip-dev.txt

CMD run
```````

Run server

.. code-block:: bash

    $ sudo /_opt/python-env/castlabs/bin/python3 \
        /_opt/bitbucket.org/castlabs/castlabs/twisted_implementation.py 8841


Docker
``````

Docker build

.. code-block:: bash

    $ TAG_VERSION=0.1
    $ docker build -t adan/castlabs:${TAG_VERSION} .

Docker push

.. code-block:: bash

    $ docker push adan/castlabs:${TAG_VERSION} .


Docker run as demon

.. code-block:: bash

    $ docker run -e PROXY_PORT=8841 -p 8841:8841 -d adan/castlabs:${TAG_VERSION}

    #run as interactive (for debug purposes)
    $ docker run -e PROXY_PORT=8841 -p 8841:8841 -it adan/castlabs:${TAG_VERSION} bash

Docker composer

.. code-block:: bash

    $ docker-composer up


Test if is work
```````````````

.. code-block:: bash

    $ curl http://127.0.0.1:8841/status                                             [0:09:17]
    {
        "ok": {
            "code": 200,
            "message": "ok"
        },
        "transferred_bytes": 557,
        "uptime": "0:01:50.536777"
    }%


    $ curl -x http://127.0.0.1:8841 \                                               [0:11:02]
        --range 0-99 http://www.nominatim.org/release/Nominatim-2.4.0.tar.bz2
    BZh91AY&SY+�����������������������.....�j%

    $ curl -x http://127.0.0.1:8841 \
        http://www.nominatim.org/release/Nominatim-2.4.0.tar.bz2?range=0-5
    BZh91A%

    $ curl -x http://127.0.0.1:8841 \
        --range 0-6 \
        http://www.nominatim.org/release/Nominatim-2.4.0.tar.bz2?range=0-5
    {
        "error": {
            "code": 413,
            "message": "both header and query parameter are specified,
                                            but with different value",
            "range_from_header": "0-6",
            "range_from_query": "0-5"
        }
    }%


    $ curl -x http://127.0.0.1:8841 \
        --range 0-5 \
        http://www.nominatim.org/release/Nominatim-2.4.0.tar.bz2?range=0-5
    BZh91A%

    $ curl http://127.0.0.1:8841/status                                       [0:15:40]
    {
        "ok": {
            "code": 200,
            "message": "ok"
        },
        "transferred_bytes": 1006,
        "uptime": "0:06:35.395526"
    }%

    $ curl http://127.0.0.1:8841/wrong_path                                   [0:15:47]
    {
        "error": {
            "code": 404,
            "message": "Not Found"
        },
        "path": "/wrong_path"
    }%

    # =======================================
    # compare messages with and without proxy
    # =======================================
    $ curl -x http://127.0.0.1:8841 \                                         [0:23:35]
        --range "1-2,4-5" \
        http://www.nominatim.org/release/Nominatim-2.4.0.tar.bz2

    --52b3589b82cee610f
    Content-type: application/x-bzip2
    Content-range: bytes 1-2/48178578

    Zh
    --52b3589b82cee610f
    Content-type: application/x-bzip2
    Content-range: bytes 4-5/48178578

    1A
    --52b3589b82cee610f--

    $ curl  --range "1-2,4-5" \                                               [0:23:47]
            http://www.nominatim.org/release/Nominatim-2.4.0.tar.bz2

    --52b358e36991f1f6e
    Content-type: application/x-bzip2
    Content-range: bytes 1-2/48178578

    Zh
    --52b358e36991f1f6e
    Content-type: application/x-bzip2
    Content-range: bytes 4-5/48178578

    1A
    --52b358e36991f1f6e--