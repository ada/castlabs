FROM python:3.5
RUN mkdir -p /_opt/castlabs
ADD . /_opt/castlabs/

WORKDIR /_opt/castlabs/

ENV PROXY_PORT 8841

RUN apt-get update && \
    apt-get install -y --force-yes \
        $(grep -oP "^[^#\s]+" requirements-apt-get.txt) && \
    apt-get autoremove -y && \
    pip install --upgrade pip virtualenv bpython && \
    virtualenv -p python3 /_opt/python-env/castlabs && \
    /_opt/python-env/castlabs/bin/pip install \
        -r requirements-pip.txt


EXPOSE ${PROXY_PORT}

CMD /_opt/python-env/castlabs/bin/python3 \
        /_opt/castlabs/castlabs/twisted_implementation.py

