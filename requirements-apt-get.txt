python-dev
python-pip
python3-dev
python3-pip

# https://en.wikipedia.org/wiki/Libffi required by pip
libffi-dev

# required by pip
libssl-dev

# http://rmcgibbo.github.io/blog/2013/05/23/faster-yaml-parsing-with-libyaml/
libyaml-dev

gcc # 'x86_64-linux-gnu-gcc': No such file or directory